package com.example.app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.app.Lists.friendList;
import com.example.app.R;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{

    private ArrayList<friendList> lists;
    private LayoutInflater inflater;
    private Context context;
    private OnNoteListener mOnNoteListener;

    public ListAdapter(ArrayList<friendList> lists, Context context, OnNoteListener onNoteListener){
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.lists = lists;
        this.mOnNoteListener = onNoteListener;
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = inflater.inflate(R.layout.friendlistelement, null);
        return new ListAdapter.ViewHolder(view, mOnNoteListener);
    }


    @Override
    public void onBindViewHolder(final ListAdapter.ViewHolder holder, final int position){
        holder.bindData(lists.get(position));
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView listName, tag;
        OnNoteListener onNoteListener;

        ViewHolder(View itemView, OnNoteListener onNoteListener){
            super(itemView);
            listName = itemView.findViewById(R.id.txtListName);
            tag = itemView.findViewById(R.id.txtListTag);
            this.onNoteListener = onNoteListener;

            itemView.setOnClickListener(this);
        }

        void bindData(final friendList list){
            listName.setText(list.getListName());
            tag.setText(list.getTag().getName());
        }

        @Override
        public void onClick(View v) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    public interface OnNoteListener{
        void onNoteClick(int position);
    }
}
