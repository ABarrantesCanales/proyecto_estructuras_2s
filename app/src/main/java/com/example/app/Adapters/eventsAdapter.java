package com.example.app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.app.R;
import com.example.app.Utilities.Event;

import java.util.ArrayList;

public class eventsAdapter extends RecyclerView.Adapter<eventsAdapter.ViewHolder> {

    private ArrayList<Event> events;
    private LayoutInflater inflater;
    private Context context;
    OnNoteListener mOnNoteListener;

    public eventsAdapter(ArrayList<Event> events, Context context, OnNoteListener onNoteListener) {
        this.events = events;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.mOnNoteListener = onNoteListener;
    }

    @Override
    public int getItemCount(){return events.size();}

    @Override
    public eventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = inflater.inflate(R.layout.eventslayout, null);
        return new eventsAdapter.ViewHolder(view, mOnNoteListener);
    }

    @Override
    public void onBindViewHolder(final eventsAdapter.ViewHolder holder, final int position){
        holder.bindData(events.get(position));
    }

    public void setItems(ArrayList<Event> items){ events = items;}

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView eventName, eventOwner;
        OnNoteListener onNoteListener;

        ViewHolder(View itemView, OnNoteListener onNoteListener) {
            super(itemView);
            eventName = itemView.findViewById(R.id.txtEventName);
            eventOwner = itemView.findViewById(R.id.eventOwner);
            this.onNoteListener = onNoteListener;

            itemView.setOnClickListener(this);

        }

        public void bindData(Event event) {
            eventName.setText(event.getName());
            eventOwner.setText(event.getOwner().getName());
        }

        @Override
        public void onClick(View v) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    public interface OnNoteListener{
        void onNoteClick(int position);
    }
}
