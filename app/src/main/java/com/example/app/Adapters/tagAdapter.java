package com.example.app.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.R;

public class tagAdapter extends RecyclerView.Adapter<tagAdapter.ViewHolder> {

    //public tagAdapter(ArrayList<>)

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.taglists,
                parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }



    public static  class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textName;
        public TextView textTag;
        public TextView textMembers;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.textListName);
            textTag = itemView.findViewById(R.id.textTag);
            textMembers = itemView.findViewById(R.id.textListName);
        }
    }

}
