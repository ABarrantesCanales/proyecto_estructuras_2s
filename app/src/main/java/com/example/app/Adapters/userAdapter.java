package com.example.app.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.Lists.User;
import com.example.app.R;

import java.util.ArrayList;

public class userAdapter extends RecyclerView.Adapter<userAdapter.viewHolder> {

    private ArrayList<User> userArrayList;
    private OnItemCheckListener onItemCheckListener;


    public userAdapter(ArrayList<User> users, OnItemCheckListener onItemCheckListener){
        userArrayList = users;
        this.onItemCheckListener = onItemCheckListener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.userslayout, parent, false);
        viewHolder vH = new viewHolder(view);
        return vH;
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, int position) {
        final User currentUser = userArrayList.get(position);

        holder.textView1.setText(currentUser.getName());
        holder.textView2.setText(Integer.toString(currentUser.getId()));

        ((viewHolder) holder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((viewHolder) holder).checkBox.setChecked(
                        !((viewHolder) holder).checkBox.isChecked());
                if(holder.checkBox.isChecked()){
                    onItemCheckListener.onItemCheck(currentUser);
                }else{
                    onItemCheckListener.onItemCheck(currentUser);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return userArrayList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder{

        public TextView textView1;
        public TextView textView2;
        public CheckBox checkBox;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(R.id.txtUserName);
            textView2 = itemView.findViewById(R.id.txtUserInfo);
            checkBox = itemView.findViewById(R.id.checkBox);
            checkBox.setClickable(false);
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }

    }

    public interface OnItemCheckListener{
        void onItemCheck(User user);
        void onItemUncheck(User user);
    }

}
