package com.example.app.Utilities;

import com.example.app.Lists.User;
import com.example.app.Lists.friendList;


public class Event {

    public User owner;
    public friendList friendList;
    public String name;
    public String description;

    public Event(User user, friendList friendList, String name, String description){
        this.owner = user;
        this.friendList = friendList;
        this.name = name;
        this.description = description;
    }

    public User getOwner() {
        return owner;
    }

    public com.example.app.Lists.friendList getFriendList() {
        return friendList;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
