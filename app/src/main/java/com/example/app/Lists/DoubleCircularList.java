package com.example.app.Lists;

import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.app.Activities.SignIn;
import com.example.app.Utilities.utilidades;

import java.io.Serializable;
import java.util.ArrayList;

/*
Data structure used to store users
*/
public class DoubleCircularList extends AppCompatActivity implements Serializable {


    public User first, last;
    public static DoubleCircularList singletonObject;

    /*
    Design pattern that only allows a single instance of the object to be created
    */
    public static synchronized DoubleCircularList getUserList(){
        if(singletonObject == null){
            singletonObject = new DoubleCircularList();
        }
        return singletonObject;
    }

    /*
    Add users in ascending order, according to their ID;
    */
    public boolean addUser(User user){
        //User newUser = new User(name, id, password, phone);
        if(searchUser(user.getId()) == null) {
            if (first == null) {
                user.next = user;
                user.prev = user;
                first = last = user;
                return true;
            } else if (user.getId() <= first.getId()) {
                user.next = first;
                user.prev = last;
                last.next = user;
                first.prev = user;
                first = user;
                return true;
            } else if (user.getId() >= last.getId()) {
                user.next = first;
                user.prev = last;
                last.next = user;
                first.prev = user;
                last = user;
                return true;
            } else {
                User aux = first.next;
                while (aux != first) {
                    if (user.getId() <= aux.getId()) {
                        user.next = aux;
                        user.prev = aux.prev;
                        aux.prev.next = user;
                        aux.prev = user;
                        return true;
                    }
                    aux = aux.next;
                }
            }
        }
        return false;
    }

    /*
    Gets an ArrayList of all the users but the given one
    */
    public ArrayList<User> getUsersBut(User user){
        if(first == null){
            return null;
        }
        ArrayList<User> users = new ArrayList<>();
        if(first == user){
            if(first.next == first){
                return null;
            }
            User aux = first.next;
            while (aux != first){
                users.add(aux);
                aux = aux.next;
            }
            return users;
        }
        User aux = first;
        while (aux.next != first){
            if(aux != user){
                users.add(aux);
            }
            aux = aux.next;

        }
        return users;
    }

    /*
    Gets an ArrayList of all the users that have the given tag linked to their references,
    skips the given user
    */
    public ArrayList<User> getUsersByTag(User user, Tag tag){
        if(first == null){
            return null;
        }
        ArrayList<User> users = new ArrayList<>();
        if(first == user){
            if(first.next == first){
                return null;
            }
            User aux = first.next;
            while (aux != first){
                if(aux.searchLinkedTag(tag) != null){
                    users.add(aux);
                }
                aux = aux.next;
            }
            return users;
        }
        User aux = first.next;
        while (aux != first){
            if(aux != user){
                if(aux.searchLinkedTag(tag) != null){
                    users.add(aux);
                }
            }
            aux = aux.next;
        }
        return users;
    }


    /*
    Searches if a user already exists or not
    */
    public User searchUser(int id){
        if(first == null){
            return null;
        }else if(first.getId() == id){
            return first;
        }else if(first.next == first){
            return null;
        }
        User aux = first.next;
        while(aux != first){
            if(aux.getId() == id){
                return aux;
            }
            aux = aux.next;
        }
        return null;
    }
    
    /*
    Modifies a given user data
    */
    public boolean modify(String name, int id, int phone){
        User user = searchUser(id);
        if(user != null){
            user.setName(name);
            //user.setLocation(location);
            user.setPhone(phone);
            return true;
        }
        return false;
    }
    
    /*
    Deletes the user that has the given ID
    */
    public boolean deleteUser(int id){
        User user = searchUser(id);
        if(user != null){
            if(user == first){
                if(first.next == first){
                    first = last = null;
                    return true;
                }
                first = first.next;
                last.next = first;
                first.prev = last;
                return true;
            }else if(user == last){
                last.prev.next = first;
                last = last.prev;
                first.prev = last;
                return true;
            }
            user.prev.next = user.next;
            user.next.prev = user.prev;
            return true;
        }
        return false;
    }

    private void limpiar() {

        SignIn a = new SignIn();

        a.name.setText("");
        a.password.setText("");
        a.location.setText("");
        a.phone.setText("");


    }
}
