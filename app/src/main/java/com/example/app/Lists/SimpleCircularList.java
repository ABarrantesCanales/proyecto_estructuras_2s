package com.example.app.Lists;

import java.io.Serializable;

public class SimpleCircularList implements Serializable {
    
    public Tag first;
    public Tag last;
    public static SimpleCircularList singletonObject;

    public static synchronized SimpleCircularList getTagList(){
        if(singletonObject == null){
            singletonObject = new SimpleCircularList();
        }
        return singletonObject;
    }

    /*
    Adds a new tag at the beginning of the list
    */
    public boolean addTag(String name, String description){
        Tag tag = new Tag(name, description);
        if(first == null){
            first = last = tag;
            tag.next = first;
            return true;
        }else if(searchTag(name) == null){
            last.next = tag;
            tag.next = first;
            first = tag;
            return true;
        }
        return false;
    }
    
    /*
    Searches if a tag already exists or not
    */
    public Tag searchTag(String name){
        if(first == null) {
            return null;
        }else if(first.getName().equalsIgnoreCase(name)){
            return first;
        }else if(first.next == first){
            return null;
        }
        Tag aux = first.next;
        while(aux != first){
            if(aux.getName().equalsIgnoreCase(name)){
                return aux;
            }
            aux = aux.next;
        }
        return null;
    }
    
    /*
    Deletes a tag, and changes its references
    */
    public boolean deleteTag(String name){
        Tag tag = searchTag(name);
        if(tag != null){
            if(tag == first){
                if(first.next == first){
                    first = last = null;
                    return true;
                }
                first = first.next;
                last.next = first;
                return true;
            }
            Tag aux = first;
            while (aux.next != tag){
                aux = aux.next;
            }
            if(tag == last){
                aux.next = first;
                last = aux;
                return true;
            }
            aux.next = tag.next;
            return true;
        }
        return false;
    }
}
