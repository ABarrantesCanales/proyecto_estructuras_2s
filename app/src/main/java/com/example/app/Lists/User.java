package com.example.app.Lists;

import com.example.app.Utilities.Event;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class User {
    private String name;
    private int id;
    private String password;
    private MarkerOptions markerOptions;
    private int phone;
    public ArrayList<friendList> friendLists;
    public ArrayList<Event> ownEvents, invitedEvent;
    public User next, prev;
    public DoubleList friends;
    public linkedTag linkedtags;

    public User(String name, int id, String password, MarkerOptions markerOptions, int phone) {
        this.name = name;
        this.id = id;
        this.password = password;
        this.markerOptions = markerOptions;
        this.phone = phone;
        this.friends = new DoubleList();
        this.friendLists = new ArrayList<>();
        this.ownEvents = new ArrayList<>();
        this.invitedEvent = new ArrayList<>();
    }

    public User(String name, int id, String password, int phone) {
        this.name = name;
        this.id = id;
        this.password = password;
        this.phone = phone;
        this.friends = new DoubleList();
        this.friendLists = new ArrayList<>();
    }

    public boolean addLiking(String tagName){
        Tag tag = SimpleCircularList.getTagList().searchTag(tagName);
        if(tag != null){
            if(linkedtags == null){
                linkedTag link = new linkedTag();
                link.setTag(tag);
                linkedtags = link;
                return true;
            }else if(searchLinkedTag(tag) == null){
                linkedTag link = new linkedTag();
                link.setTag(tag);
                link.next = linkedtags;
                linkedtags = link;
            }
        }
        return false;
    }

    public void addOwnEvent(Event event){
        this.ownEvents.add(event);
        ArrayList<User> friends = event.getFriendList().getFriends();
        for(User user: friends){
            user.addInvitedEvent(event);
        }
    }

    public void addInvitedEvent(Event event){
        this.invitedEvent.add(event);
    }

    public linkedTag searchLinkedTag(Tag tag) {
        linkedTag aux = linkedtags;
        while (aux != null) {
            if (aux.getTag() == tag) {
                return aux;
            }
            aux = aux.next;
        }
        return null;
    }

    public void addFriendList(friendList list){
        friendLists.add(list);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }
    

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getPassword(){
        return password;
    }

    public MarkerOptions getMarkerOptions() {
        return markerOptions;
    }

    public int getPhone() {
        return phone;
    }

    public ArrayList<Event> getOwnEvents() {
        return ownEvents;
    }

    public ArrayList<Event> getInvitedEvent() {
        return invitedEvent;
    }

    public friendList getListByName(String name){
        for(friendList f: friendLists){
            if(f.getListName().equals(name)){
                return f;
            }
        }
        return null;
    }

    public ArrayList<friendList> getFriendLists() {
        return friendLists;
    }
}
