package com.example.app.Lists;

import java.util.ArrayList;

public class friendList {

    private ArrayList<User> friends;
    private String listName;
    private Tag tag;

    public friendList(String name, ArrayList<User> friends, Tag tag){
        this.friends = friends;
        this.listName = name;
        this.tag = tag;
    }

    public ArrayList<User> getFriends() {
        return friends;
    }

    public String getListName() {
        return listName;
    }

    public Tag getTag() {
        return tag;
    }
}
