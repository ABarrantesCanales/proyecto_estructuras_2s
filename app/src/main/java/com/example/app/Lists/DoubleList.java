package com.example.app.Lists;

import java.util.ArrayList;

/*
Data Structure used store the friends of a given user
*/
public class DoubleList {
    
    public Friend first, last;

    /*
    Adds a friend at the end of the list
    */
    public boolean addFriend(String date, Tag tag, User friendo){
        Friend friend = new Friend(date,tag,friendo);
        if(first == null){
            friend.next = null;
            friend.prev = null;
            first = last = friend;
            return true;
        }else if(searchFriend(friendo.getName()) == null){
            last.next = friend;
            friend.prev = last;
            friend.next = null;
            last = friend;
            return true;
        }
        return false;
    }

    public boolean addFriend(Friend friend){
        if(first == null){
            friend.next = null;
            friend.prev = null;
            first = last = friend;
            return true;
        }else if(searchFriend(friend.getFriendo().getName()) == null){
            last.next = friend;
            friend.prev = last;
            friend.next = null;
            last = friend;
            return true;
        }
        return false;
    }
    
    public ArrayList<Friend> friendsByTag(Tag tag) {
        if (first == null) {
            return null;
        }else if (first.next == null) {
            if (first.getTag() == tag) {
                ArrayList<Friend> friends = new ArrayList<>();
                friends.add(first);
                return friends;
            }
            return null;
        }
        ArrayList<Friend> friends = new ArrayList<>();
        Friend aux = first.next;
        while (aux != null) {
            if (aux.getTag() == tag) {
                friends.add(aux);
            }
            aux = aux.next;
        }
        return friends;
    }

    /*
    
    */
    public Friend searchFriend(String name) {
        if(first == null){
            return null;
        }else if(first.getFriendo().getName().equalsIgnoreCase(name)){
            return first;
        }else if(first.next == null){
            return null;
        }
        Friend aux = first.next;
        while(aux != null){
            if(aux.getFriendo().getName().equalsIgnoreCase(name)){
                return aux;
            }
            aux = aux.next;
        }
        return null;
    }

//    public boolean searchListName(String name){
//        if(first == null){
//            return false;
//        }else if(first.searchListName(name)){
//            return true;
//        }else if(first.next == null){
//            return false;
//        }
//        Friend aux = first.next;
//        while(aux != null){
//            if(aux.searchListName(name)){
//                return true;
//            }
//            aux = aux.next;
//        }
//        return false;
//    }

    public boolean deleteFriend(String name){
        Friend friend = searchFriend(name);
        if(friend != null){
            if(friend == first){
                if(first.next == null){
                    first = last = null;
                    return true;
                }
                first = first.next;
                first.prev = null;
                return true;
            }else if(friend == last){
                last = last.prev;
                last.next = null;
                return true;
            }
            friend.prev.next = friend.next;
            friend.next.prev = first.prev;
            return true;
        }
        return false;
    }
    
}


