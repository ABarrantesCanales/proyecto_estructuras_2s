package com.example.app.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.Activities.MainActivity;
import com.example.app.Activities.eventCreation;
import com.example.app.Adapters.ListAdapter;
import com.example.app.Lists.friendList;
import com.example.app.R;

import java.util.ArrayList;


public class friendLists extends Fragment implements com.example.app.Adapters.ListAdapter.OnNoteListener {


    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public RecyclerView recyclerView;
    public ListAdapter ListAdapter;
    private ArrayList<friendList> lists;


    private String mParam1;
    private String mParam2;

    public friendLists() {
    }

    public static friendLists newInstance(String param1, String param2) {
        friendLists fragment = new friendLists();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend, container, false);
        ListAdapter = new ListAdapter(((MainActivity)getActivity()).actualUser.getFriendLists(), getContext(), this);
        lists = ((MainActivity)getActivity()).actualUser.getFriendLists();
        recyclerView = view.findViewById(R.id.friendRecycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(ListAdapter);


        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        ListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onNoteClick(int position) {
        Intent intent = new Intent(getActivity(), eventCreation.class);
        intent.putExtra("listName", lists.get(position).getListName());
        int id = ((MainActivity)getActivity()).actualUser.getId();
        intent.putExtra("user", Integer.toString(id));
        startActivity(intent);
        System.out.println("Clicked");
    }
}