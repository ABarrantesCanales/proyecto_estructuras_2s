package com.example.app.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.example.app.Activities.MainActivity;
import com.example.app.Activities.linkTags;
import com.example.app.Activities.listCreation;
import com.example.app.Activities.tagCreation;
import com.example.app.Lists.User;
import com.example.app.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link create#newInstance} factory method to
 * create an instance of this fragment.
 */
public class create extends Fragment implements View.OnClickListener {

    View view;
    Button btnTag;
    Button btnList;
    Button btnAddtag;


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    public create() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment createList.
     */

    public static create newInstance(String param1, String param2) {
        create fragment = new create();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final User actualUser = ((MainActivity)getActivity()).actualUser;
        view = inflater.inflate(R.layout.fragment_create, container, false);
        btnTag = view.findViewById(R.id.btnCreateTag);
        btnList = view.findViewById(R.id.btnCreateList);
        btnAddtag = view.findViewById(R.id.btnAddTag);
        btnTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), tagCreation.class);
                startActivity(intent);
            }
        });
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), listCreation.class);
                intent.putExtra("UserID", (Integer.toString(actualUser.getId())));
                startActivity(intent);
            }
        });
        btnAddtag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), linkTags.class);
                intent.putExtra("UserID", (Integer.toString(actualUser.getId())));
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {

    }

//    public void onClick(View view){
//        switch (view.getId()){
//            case R.id.btnCreateTag:
//                System.out.println("crear tags");
//                break;
//            case R.id.btnCreateList:
//                System.out.println("crear listas");
//                break;
//        }
//    }
}