package com.example.app.Graph;

import com.example.app.Lists.User;

public class Vertice {

    User user;
    Vertice prev, next;
    Edge edges;


    public Vertice(User user){
        this.user = user;
    }

    public void addEdge(Edge e){
        if(edges == null){
            edges = e;
        }else{
            edges.prev = e;
            e.next = edges;
            edges = e;
        }
    }

    public User getUser(){
        return user;
    }
}
