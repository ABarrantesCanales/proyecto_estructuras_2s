package com.example.app.Graph;

import com.example.app.Lists.User;

import java.util.Random;

public class Graph {

    Vertice head;
    Edge edgesHead;
    public static Graph instance;

    /*
    Design pattern that only allows a single instance of the object to be created
    */
    public static synchronized Graph getGraph(){
        if(instance == null){
            instance = new Graph();
        }
        return instance;
    }


    public boolean insertVertice(User user){
        if(head == null){
            Vertice nV = new Vertice(user);
            head = nV;
            return true;
        }else if(searchVertice(user) == null){
            Vertice nV = new Vertice(user);
            head.prev = nV;
            nV.next = head;
            head = nV;
            return true;
        }
        return false;
    }

    public boolean insertEdge(Vertice origin, Vertice dest, int weight){
        if(origin != null || dest != null){
            if(edgesHead == null){
                Edge edge = new Edge(weight, origin, dest);
                edgesHead = edge;
                origin.addEdge(edge);
                return true;
            }else if(searchEdge(origin, dest) == null){
                Edge edge = new Edge(weight, origin, dest);
                edgesHead.prev = edge;
                edge.next = edgesHead;
                edgesHead = edge;
                origin.addEdge(edge);
                return true;
            }
        }
        return false;
    }

    public Vertice searchVertice(User user){
        Vertice aux = head;
        while(aux != null){
            if(aux.getUser().getName().equals(user.getName())){
                return aux;
            }
            aux = aux.next;
        }
        return null;
    }

    public Edge searchEdge(Vertice origin, Vertice dest){
        Edge aux = edgesHead;
        while(aux != null){
            if(aux.source == origin && aux.dest == dest){
                return aux;
            }
            aux = aux.next;
        }

        return null;
    }

    public void connectGraph(){
        if(head == null || head.next == null){
            return;
        }
        Vertice aux = head;
        Vertice aux2 = head;
        Random r = new Random();
        while (aux != null){
            while (aux2 != null){
                if(aux != aux2){
                    if(searchEdge(aux, aux2) == null){
                        insertEdge(aux, aux2,  r.nextInt(100) + 1);
                    }
                }
                aux2 = aux2.next;

            }
            aux2 = head;
            aux = aux.next;

        }
    }







}
