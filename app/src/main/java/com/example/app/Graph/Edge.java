package com.example.app.Graph;

public class Edge {
    int weight;
    Edge prev, next;
    Vertice source, dest;

    public Edge(int weight, Vertice source, Vertice dest) {
        this.weight = weight;
        this.source = source;
        this.dest = dest;
    }

    public int getWeight() {
        return weight;
    }

    public Vertice getSource() {
        return source;
    }

    public Vertice getDest() {
        return dest;
    }
}
