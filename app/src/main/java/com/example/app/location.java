package com.example.app;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.app.Activities.mapView;

public class location implements LocationListener {

    mapView myMapView;
    TextView tvMensaje;

    public mapView getMainActivity() {
        return myMapView;
    }

    public void setMainActivity(mapView mapView, TextView tvMensaje) {
        this.myMapView = mapView;
        this.tvMensaje = tvMensaje;
    }

    @Override
    public void onLocationChanged(Location location) {
        // Este metodo se ejecuta cuando el GPS recibe nuevas coordenadas

        String texto = "Mi ubicación es: \n"
                + "Latitud = " + location.getLatitude() + "\n"
                + "Longitud = " + location.getLongitude();

        tvMensaje.setText(texto);

        //mapa(location.getLatitude(), location.getLongitude());
    }

//    public void mapa(double lat, double lon) {
//        // Fragment del Mapa
//        mapFragment fragment = new mapFragment();
//
//        Bundle bundle = new Bundle();
//        bundle.putDouble("lat", new Double(lat));
//        bundle.putDouble("lon", new Double(lon));
//        fragment.setArguments(bundle);
//
//        FragmentManager fragmentManager = getMainActivity().getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.add(R.id.fragment, fragment, null);
//        fragmentTransaction.commit();
//    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case LocationProvider.AVAILABLE:
                Log.d("debug", "LocationProvider.AVAILABLE");
                break;
            case LocationProvider.OUT_OF_SERVICE:
                Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                break;
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        tvMensaje.setText("GPS Activado");
    }

    @Override
    public void onProviderDisabled(String provider) {
        tvMensaje.setText("GPS Desactivado");
    }
}
