package com.example.app.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.app.Lists.DoubleCircularList;
import com.example.app.Lists.User;
import com.example.app.R;
import com.example.app.Utilities.Event;

public class eventCreation extends AppCompatActivity {

    DoubleCircularList users = DoubleCircularList.getUserList();
    private User actualUser;
    private String listName;

    private TextView txtName;
    private TextView txtDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_creation);

        Intent intent = getIntent();
        listName = intent.getStringExtra("listName");
        int id = Integer.parseInt(intent.getStringExtra("user"));
        actualUser = users.searchUser(id);
        txtName = findViewById(R.id.txtEventName);
        txtDesc = findViewById(R.id.txtEventDesc);
    }

    public void createEventClick(View view){
        AlertDialog.Builder emptyAlert = new AlertDialog.Builder(this);
        emptyAlert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        if(!txtName.getText().equals("")){
            if(!txtDesc.getText().equals("")){
                Event event = new Event(actualUser, actualUser.getListByName(listName),
                        txtName.getText().toString(), txtDesc.getText().toString());
                actualUser.addOwnEvent(event);
                AlertDialog.Builder ok = new AlertDialog.Builder(this);
                ok.setMessage("Event created!");
                ok.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                ok.create().show();
            }else {
                emptyAlert.setMessage("Please enter a valid description.");
                emptyAlert.create().show();
            }
        }else {
            emptyAlert.setMessage("Please enter a valid Name.");
            emptyAlert.create().show();
        }
    }


    public void backClick(View view){
        finish();
    }
}