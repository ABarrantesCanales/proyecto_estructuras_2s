package com.example.app.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.app.Lists.DoubleCircularList;
import com.example.app.Lists.SimpleCircularList;
import com.example.app.Lists.User;
import com.example.app.R;

import java.io.Serializable;

public class LogIn extends AppCompatActivity implements Serializable {

    private EditText id;
    private EditText password;
    DoubleCircularList users = DoubleCircularList.getUserList();
    SimpleCircularList tags = SimpleCircularList.getTagList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        id = (EditText)findViewById(R.id.txtName);
        password = (EditText)findViewById(R.id.txtUserPw);
        //createDummyObj(); // Todo eliminar este metodo despues de pruebas
    }

    public void loginClick(View view){
        if(users.first == null){
            AlertDialog.Builder emptyAlert = new AlertDialog.Builder(this);
            emptyAlert.setMessage("There are no users, you should " +
                    "create one before accessing the app. ");
            emptyAlert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    id.setText("");
                    password.setText("");
                }
            });
            emptyAlert.create().show();
        }else{
            User user = users.searchUser(Integer.parseInt(id.getText().toString()));
            if(user != null){
                if(user.getPassword().equals(password.getText().toString())){
                    Intent main = new Intent(this, MainActivity.class);
                    main.putExtra("UserID", id.getText().toString());
                    startActivity(main);
                }else{
                    AlertDialog.Builder noUser = new AlertDialog.Builder(this);
                    //noUser.setTitle("Wrong data");
                    noUser.setMessage("Please check your data as some of it is wrong.");
                    noUser.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            id.setText("");
                            password.setText("");
                        }
                    });
                    noUser.create().show();

                }
            }else{
                AlertDialog.Builder noUser = new AlertDialog.Builder(this);
                noUser.setTitle("User not found");
                noUser.setMessage("The given ID does not belong to any user.");
                noUser.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        id.setText("");
                        password.setText("");
                    }
                });
                noUser.create().show();
            }
        }
    }

    public void signInClick(View view){
        Intent signIn = new Intent(this, SignIn.class);
        startActivity(signIn);
    }

//    public void createDummyObj(){//todo eliminarme
//        users.addUser("josue", 1, "q",  0);
//        users.addUser("Peje", 12332, "q",  0);
//        users.addUser("Krlos", 1231, "q",  0);
//        users.addUser("aaa", 666, "q",  0);
//        users.addUser("pepo", 69420, "q",  0);
//
//        tags.addTag("anime", "anime");
//        tags.addTag("series", "series");
//        tags.addTag("deportes", "deportes");
//        tags.addTag("pelis", "pelis");
//        tags.addTag("gaming", "gaming");
//        tags.addTag("natacion", "natacion");
//
//        users.searchUser(1).addLiking("anime");
//        users.searchUser(12332).addLiking("anime");
//        users.searchUser(12332).addLiking("pelis");
//        users.searchUser(1231).addLiking("anime");
//        users.searchUser(666).addLiking("deportes");
//        users.searchUser(1231).addLiking("gaming");
//        users.searchUser(12332).addLiking("series");
//        users.searchUser(69420).addLiking("anime");
//        users.searchUser(666).addLiking("series");
//
//    }
}




