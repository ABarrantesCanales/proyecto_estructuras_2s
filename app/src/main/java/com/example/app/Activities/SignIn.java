package com.example.app.Activities;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.app.Graph.Graph;
import com.example.app.Lists.DoubleCircularList;
import com.example.app.Lists.User;
import com.example.app.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;

public class SignIn extends AppCompatActivity implements Serializable {

    public EditText name;
    public EditText id;
    public EditText password;
    public static TextView location;
    public EditText phone;
    public Button btnLocation;
    public static double latitude = 0;
    public static double longitude = 0;

    DoubleCircularList users = DoubleCircularList.getUserList();
    Graph graph = Graph.getGraph();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        name = (EditText)findViewById(R.id.txtNameS);
        id = (EditText)findViewById(R.id.txtIDS);
        password = (EditText)findViewById(R.id.txtPasswordS);
        location = (TextView) findViewById(R.id.txtLocation);
        phone = (EditText)findViewById(R.id.txtPhone);
        btnLocation = (Button)findViewById(R.id.btnLocation);

    }

    public void signInClick(View view){
        AlertDialog.Builder emptyAlert = new AlertDialog.Builder(this);
        emptyAlert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        if(!name.getText().toString().equals("")) {
            if (!id.getText().toString().equals("")) {
                if(users.searchUser(Integer.parseInt(id.getText().toString())) == null){
                    System.out.println(users.getClass().toString());
                    if (!password.getText().toString().equals("")) {
                        if (!phone.getText().toString().equals("")) {
                            if(latitude != 0 || longitude != 0){
                                LatLng latLng = new LatLng(latitude, longitude);
                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(latLng);
                                User user = new User(name.getText().toString(), Integer.parseInt(id.getText().toString()),
                                        password.getText().toString(), markerOptions,
                                        Integer.parseInt(phone.getText().toString()));
                                users.addUser(user);
                                graph.insertVertice(user);
                                graph.connectGraph();
                                Registrar(view);
                                AlertDialog.Builder ok = new AlertDialog.Builder(this);
                                ok.setMessage("User created!");
                                ok.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                        latitude = 0;
                                        longitude = 0;
                                    }
                                });
                                ok.create().show();
                            }else{
                                emptyAlert.setMessage("Please set your location.");
                                emptyAlert.create().show();
                            }

                        }else{
                            emptyAlert.setMessage("Please enter a valid phone number.");
                            emptyAlert.create().show();
                        }
                    }else {
                        emptyAlert.setMessage("Please enter a valid password.");
                        emptyAlert.create().show();
                    }

                }else{
                    emptyAlert.setMessage("The given ID already exists, please try a new one.");
                    emptyAlert.create().show();
                    id.setText("");
                }
            }else{
                emptyAlert.setMessage("Please enter a valid ID.");
                emptyAlert.create().show();
            }
        }else{
            emptyAlert.setMessage("Please enter a valid user.");
            emptyAlert.create().show();
        }
    }

    public  void Registrar(View view){


        AdminSqlite admin= new AdminSqlite(this,"administrador",null,1);
        SQLiteDatabase baseDeDatos = admin.getReadableDatabase();
        String name1 = name.getText().toString();
        String id1 = id.getText().toString();
        String password1 = password.getText().toString();
        String location1 = location.getText().toString();
        String phone1 = phone.getText().toString();



        ContentValues registro = new ContentValues();
        registro.put("name1",name1);
        registro.put("id1", id1);
        registro.put("password1",password1);
        registro.put("location1",location1);
        registro.put("phone1",phone1);




        baseDeDatos.insert("user", null,registro);
        Toast.makeText(this,"BUENISIMA", Toast.LENGTH_SHORT).show();
        baseDeDatos.close();


    }

    public void clickLocation(View view){
        Intent intent = new Intent(this, mapView.class);
        startActivity(intent);
    }

    public static void setCoordinates(){
        location.setText(latitude + "\n" + longitude);
    }

}