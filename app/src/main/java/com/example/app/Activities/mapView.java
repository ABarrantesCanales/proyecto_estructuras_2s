package com.example.app.Activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.app.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class mapView extends AppCompatActivity implements OnMapReadyCallback {

    public TextView tvMensaje;
    public Button btnGetCoordinates;
    public GoogleMap map;
    public MarkerOptions markerOpts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);

        tvMensaje = findViewById(R.id.tvMensaje);
        btnGetCoordinates = findViewById(R.id.btnLocation);


        SupportMapFragment supportMapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        supportMapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(latLng.latitude + " : " + latLng.longitude);
                mapView.this.markerOpts = markerOptions;
                tvMensaje.setText(markerOpts.getPosition().latitude + " ; "
                        + markerOpts.getPosition().longitude);
                map.clear();
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                map.addMarker(markerOptions);
            }
        });
    }

    public void getCoordinatesClk(View view){
        final AlertDialog.Builder emptyAlert = new AlertDialog.Builder(this);
        emptyAlert.setNeutralButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tvMensaje.setText("Coordinates: ");
            }
        });
        emptyAlert.setMessage("Is this direction correct?" +
                markerOpts.getPosition().latitude + " ; " + markerOpts.getPosition().longitude);
        emptyAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SignIn.latitude = markerOpts.getPosition().latitude;
                SignIn.longitude = markerOpts.getPosition().longitude;
                SignIn.setCoordinates();
                finish();
            }
        });
        emptyAlert.create().show();

    }
}