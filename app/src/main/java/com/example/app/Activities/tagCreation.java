package com.example.app.Activities;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.app.Lists.SimpleCircularList;
import com.example.app.R;
import com.example.app.Utilities.utilidades;

public class tagCreation extends AppCompatActivity {

    private EditText tagName;
    private EditText tagDesc;
    private Button btnCreate;
    SignIn a = new SignIn();

    SimpleCircularList tags = SimpleCircularList.getTagList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_creation);

        tagName = findViewById(R.id.txtTag);
        tagDesc = findViewById(R.id.txtDesc);
        btnCreate = findViewById(R.id.btnCreateT);
        a.name = (EditText)findViewById(R.id.txtNameS);
        a.id = (EditText)findViewById(R.id.txtIDS);
        a.password = (EditText)findViewById(R.id.txtPasswordS);
        a.phone = (EditText)findViewById(R.id.txtPhone);

    }

    public void backClick(View view){
        finish();
    }

    public void createClick(View view){
        AlertDialog.Builder emptyAlert = new AlertDialog.Builder(this);
        emptyAlert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        if(!tagName.getText().toString().equals("")){
            if(tags.searchTag(tagName.getText().toString()) == null){
                if(!tagDesc.getText().toString().equals("")){
                    tags.addTag(tagName.getText().toString(), tagDesc.getText().toString());
                    RegistrarTag(view);
                    AlertDialog.Builder ok = new AlertDialog.Builder(this);
                    ok.setMessage("Tag created!");
                    ok.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    ok.create().show();
                }else{
                    emptyAlert.setMessage("Please enter a valid description.");
                    emptyAlert.create().show();
                }
            }else{
                emptyAlert.setMessage("The given Tag already exists, please create a new one.");
                emptyAlert.create().show();
                tagName.setText("");
                tagDesc.setText("");
            }
        }else{
            emptyAlert.setMessage("Please enter a valid Tag name.");
            emptyAlert.create().show();
        }
    }


    public  void RegistrarTag(View view ){


        tagSqlite admin= new tagSqlite(this,"Tagadministrador",null,1);
        SQLiteDatabase baseDeDatos = admin.getReadableDatabase();
        String namTag = tagName.getText().toString();
        String desTag = tagDesc.getText().toString();


        ContentValues registro = new ContentValues();
        registro.put("namTag",namTag);
        registro.put("desTag", desTag);



        baseDeDatos.insert("user", null,registro);
        Toast.makeText(this,"BUENISIMA", Toast.LENGTH_SHORT).show();
        baseDeDatos.close();

    }
}