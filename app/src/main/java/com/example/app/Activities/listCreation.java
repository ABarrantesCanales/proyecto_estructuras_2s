package com.example.app.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.Lists.DoubleCircularList;
import com.example.app.Lists.SimpleCircularList;
import com.example.app.Lists.Tag;
import com.example.app.Lists.User;
import com.example.app.Lists.friendList;
import com.example.app.R;
import com.example.app.Adapters.userAdapter;

import java.util.ArrayList;
import java.util.List;

public class listCreation extends AppCompatActivity {

    SimpleCircularList tags = SimpleCircularList.getTagList();
    DoubleCircularList users = DoubleCircularList.getUserList();
    private User actualUser;

    private Spinner spinnerTags;
    private EditText listName;
    private RecyclerView usersRecycler;
    private RecyclerView.Adapter usersAdapter;
    private RecyclerView.LayoutManager usersLayoutManager;
    private ArrayList<User> selectedUsers = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_creation);

        spinnerTags = findViewById(R.id.spinnerTags);
        listName = findViewById(R.id.txtListName);

        Intent intent = getIntent();
        String userId = intent.getStringExtra("UserID");
        assert userId != null;
        actualUser = users.searchUser(Integer.parseInt(userId));

        checkTags();

        ArrayList<User> usersArray = users.getUsersBut(actualUser);
        usersRecycler = findViewById(R.id.btnSelectedTag);
        usersRecycler.setHasFixedSize(true);
        usersRecycler.setEnabled(false);
        usersLayoutManager = new LinearLayoutManager(this);
        usersRecycler.setLayoutManager(usersLayoutManager);
    }

    public void checkTags(){
        if(tags.first != null){
            List<Tag> tagsArray = new ArrayList<>();
            Tag aux = tags.first.next;
            tagsArray.add(tags.first);
            while (aux != tags.first){
                tagsArray.add(aux);
                aux = aux.next;
            }
            ArrayAdapter<Tag> adapter = new ArrayAdapter<Tag>(this,
                    android.R.layout.simple_spinner_item, tagsArray);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerTags.setAdapter(adapter);
        }else{
            spinnerTags.setEnabled(false);
        }
    }

    public void getSelectedTag(View view){
        Tag tag = (Tag) spinnerTags.getSelectedItem();
        final ArrayList<User> usersByTag = users.getUsersByTag(actualUser, tag);
        usersAdapter = new userAdapter(usersByTag, new userAdapter.OnItemCheckListener() {
            @Override
            public void onItemCheck(User user) {
                selectedUsers.add(user);
            }

            @Override
            public void onItemUncheck(User user) {
                selectedUsers.remove(user);
            }
        });
        usersRecycler.setAdapter(usersAdapter);
    }

    public void createFriendList(View view){
        AlertDialog.Builder emptyAlert = new AlertDialog.Builder(this);
        emptyAlert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        if(!listName.getText().toString().equals("")){
            if(!selectedUsers.isEmpty()){
                if(actualUser.searchLinkedTag((Tag)spinnerTags.getSelectedItem()) != null){
                    if(actualUser.getListByName(listName.getText().toString()) == null){
                        for(User user: selectedUsers){
                            if(actualUser.friends.searchFriend(user.getName()) == null){
                                System.out.println("Entro en if");
                                actualUser.friends.addFriend((java.time.LocalDate.now()).toString(), (Tag)spinnerTags.getTag(),
                                        user);
                            }else{
                                System.out.println("Entro en else");
                            }
                        }
                        actualUser.addFriendList(new friendList(listName.getText().toString(), selectedUsers, (Tag)spinnerTags.getSelectedItem()));
                        AlertDialog.Builder listCreated = new AlertDialog.Builder(this);
                        listCreated.setMessage("Your friend list has been created!");
                        listCreated.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                listName.setText("");

                                finish();
                            }
                        });
                        listCreated.create().show();
                    }else{
                        AlertDialog.Builder listCreated = new AlertDialog.Builder(this);
                        listCreated.setMessage("You already have a friend list with that name.\n" +
                                "Please try a new one.");
                        emptyAlert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                listName.setText("");
                            }
                        });
                    }
                }else{
                    AlertDialog.Builder unLinkedTag = new AlertDialog.Builder(this);
                    unLinkedTag.setTitle("Unlinked Tag");
                    unLinkedTag.setMessage("You don't dont have the selected tag linked to your profile.\n " +
                            "Would you like to link it and create the list?");
                    unLinkedTag.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            actualUser.addLiking(((Tag) spinnerTags.getSelectedItem()).getName());
                            for(User user: selectedUsers){
                                if(actualUser.friends.searchFriend(user.getName()) == null){
                                    actualUser.friends.addFriend((java.time.LocalDate.now()).toString(), (Tag)spinnerTags.getTag(),
                                            user);
                                }
                            }
                        }
                    });
                    unLinkedTag.setNeutralButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            listName.setText("");
                        }
                    });
                    unLinkedTag.create().show();
                }
            }else{
                emptyAlert.setMessage("Please select some users to create the list.");
                emptyAlert.create().show();
            }
        }else{
            emptyAlert.setMessage("Please enter a valid name for the list.");
            emptyAlert.create().show();
        }

    }

    public void backClick(View view){
        finish();
    }

}