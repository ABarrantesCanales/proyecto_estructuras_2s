package com.example.app.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.app.Lists.DoubleCircularList;
import com.example.app.Lists.SimpleCircularList;
import com.example.app.Lists.Tag;
import com.example.app.Lists.User;
import com.example.app.R;

import java.util.ArrayList;
import java.util.List;

public class linkTags extends AppCompatActivity {

    SimpleCircularList tags = SimpleCircularList.getTagList();
    DoubleCircularList users = DoubleCircularList.getUserList();
    private User actualUser;

    private Spinner spinnerTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_tags);

        spinnerTags = findViewById(R.id.spinnerTags2);

        Intent intent = getIntent();
        String userId = intent.getStringExtra("UserID");
        assert userId != null;
        actualUser = users.searchUser(Integer.parseInt(userId));

        checkTags();
    }

    public void checkTags(){
        if(tags.first != null){
            List<Tag> tagsArray = new ArrayList<>();
            Tag aux = tags.first.next;
            tagsArray.add(tags.first);
            while (aux != tags.first){
                tagsArray.add(aux);
                aux = aux.next;
            }
            ArrayAdapter<Tag> adapter = new ArrayAdapter<Tag>(this,
                    android.R.layout.simple_spinner_item, tagsArray);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerTags.setAdapter(adapter);
        }else{
            spinnerTags.setEnabled(false);
        }
    }

    public void addTagClick(View view){
        AlertDialog.Builder emptyAlert = new AlertDialog.Builder(this);
        emptyAlert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        if(spinnerTags.isEnabled()){
            Tag tag = (Tag) spinnerTags.getSelectedItem();
            if(actualUser.searchLinkedTag(tag) == null){
                actualUser.addLiking(tag.getName());
                AlertDialog.Builder listCreated = new AlertDialog.Builder(this);
                listCreated.setMessage("Tag added!");
                listCreated.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                listCreated.create().show();
            }else{
                emptyAlert.setMessage("You already like this tag.");
                emptyAlert.create().show();
            }
        }else{
            emptyAlert.setMessage("There are no tags.");
            emptyAlert.create().show();
        }


    }

    public void backClick(View view){
        finish();
    }
}