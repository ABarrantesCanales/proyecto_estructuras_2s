package com.example.app.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.example.app.Lists.DoubleCircularList;
import com.example.app.Lists.User;
import com.example.app.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class mapConnection extends AppCompatActivity  implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener, RoutingListener {

    DoubleCircularList users = DoubleCircularList.getUserList();

    private User actualUser;
    private User owner;
    TextView coordinates;
    public GoogleMap map;
    private List<Polyline> polylines = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_connection);
        Intent intent = getIntent();
        int ownerId = Integer.parseInt(intent.getStringExtra("user"));
        owner = users.searchUser(ownerId);
        int id = Integer.parseInt(intent.getStringExtra("user"));
        actualUser = users.searchUser(id);
        SupportMapFragment supportMapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.mapConnection);
        supportMapFragment.getMapAsync(this);

    }

    public void backClick(View view){
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        Findroutes(actualUser.getMarkerOptions().getPosition(), owner.getMarkerOptions().getPosition());

    }

    public void Findroutes(LatLng Start, LatLng End) {
        if(Start==null || End==null) {
            System.out.println("Null entries");
        }
        else {
            Routing routing = new Routing.Builder().travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this).alternativeRoutes(true).waypoints(Start, End)
                    .key("AIzaSyB_qsP8AOP_P0MdlPz-48TDaJYjTP3vbjo").build();
            routing.execute();
        }
    }


    @Override
    public void onRoutingFailure(RouteException e) {
        AlertDialog.Builder emptyAlert = new AlertDialog.Builder(this);
        emptyAlert.setNeutralButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        emptyAlert.setMessage("Route not found");
        emptyAlert.create().show();
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {

        CameraUpdate center = CameraUpdateFactory.newLatLng(actualUser.getMarkerOptions().getPosition());
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);
        if(polylines!=null) {
            polylines.clear();
        }
        PolylineOptions polyOptions = new PolylineOptions();
        LatLng polylineStartLatLng=null;
        LatLng polylineEndLatLng=null;


        polylines = new ArrayList<>();
        for (int i = 0; i <route.size(); i++) {
            if(i==shortestRouteIndex) {
                polyOptions.color(getResources().getColor(R.color.colorPrimary));
                polyOptions.width(7);
                polyOptions.addAll(route.get(shortestRouteIndex).getPoints());
                Polyline polyline = map.addPolyline(polyOptions);
                polylineStartLatLng=polyline.getPoints().get(0);
                int k=polyline.getPoints().size();
                polylineEndLatLng=polyline.getPoints().get(k-1);
                polylines.add(polyline);

            } else {

            }

        }
    }

    @Override
    public void onRoutingCancelled() {
        Findroutes(actualUser.getMarkerOptions().getPosition(), owner.getMarkerOptions().getPosition());
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Findroutes(actualUser.getMarkerOptions().getPosition(), owner.getMarkerOptions().getPosition());
    }
}